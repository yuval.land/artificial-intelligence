import numpy as np
import matplotlib.pyplot as plt

def create_game_data(game_num, turn_num, H_prior):
    H_values = [0, 1, 2, 3, 5, 7, 9]
    H = np.random.choice(H_values, game_num, p=H_prior)
    d = np.random.choice(range(1, 7), (game_num, turn_num))
    O = np.mod(d * H[:, np.newaxis], 10)
    return H_values, H, O

def tester(guess_func, game_num, turn_num, H_prior):
    assert len(H_prior.shape) == 1
    assert len(H_prior) == 7

    # create games data
    H_values, H, O = create_game_data(game_num, turn_num, H_prior)
    H_num = len(H_values)

    # play games
    Hhat = np.zeros(game_num)
    # posteriors = np.zeros((game_num, H_num))
    for j in range(game_num):
        Hhat[j] = guess_func(O[j])

    # compute error rate for each value of H
    erred = Hhat != H
    error_rate = np.zeros(H_num)
    for k, h in enumerate(H_values):
        error_rate[k] = np.mean(erred[H == h])

    # show error rates
    plt.figure()
    plt.plot(H_values, error_rate, '.')
    plt.xlabel('H value')
    plt.ylabel('Error rate')
    plt.show()
