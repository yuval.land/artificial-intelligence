#!/usr/bin/python3.6

import numpy as np
import os
import sys
import random
import colors
import time

dice = range(1, 7)  # Available values to roll
starting_possibilities = [0, 1, 2, 3, 5, 7, 9]
H_list = np.array(starting_possibilities)  # Possible Outcomes
possible_observations = range(10)
H_probability = np.array([1 / len(H_list) for _ in range(len(H_list))])


def get_count(n, l):
    """Returns the count of `n` in `l`"""
    return sum(n == x for x in l)


def get_probabilities():
    data = [[h * value % 10 for value in dice] for h in H_list]
    probabilities = []

    for row in data:
        row_probability = [2/3*(get_count(x, row) / len(dice)) + (1/3 if x in range(1, 7) else 0)
                           for x in possible_observations]
        probabilities.append(row_probability)

    return np.array(probabilities)


def numerator(H, O):
    p = get_probabilities()
    mult = 1
    for o in O:
        mult *= p[H][o]
    return H_probability[H] * mult


def new_get_prob_by_obs(H, O):
    index = np.where(H_list == H)[0][0]
    return numerator(index, O)/sum(numerator(i, O) for i in range(len(H_list)))


def get_max_guess(current_guess):
    max_probability = max(current_guess.items(), key=lambda x: x[1])
    return max_probability[0], max_probability[1]


def display_bar(value, color_values):
    spaces = 3
    print('     ', end='')
    for color in color_values[:value + 1]:
        print(f"\033[48;5;{color}m", end=' ' * spaces)
    print('\033[49m\n')


def display_data(current_guess, color_values):
    for current in starting_possibilities:
        guess = current_guess[current]
        value = int(guess * len(color_values))
        print(colors.cyan(f"{current} -> %.5f" % guess), end='')
        display_bar(value, color_values)
    biggest_number, biggest_probability = get_max_guess(current_guess)
    print(colors.red(f"\n{biggest_number} -> %.5f" % biggest_probability))
    return biggest_number


def run_game(inputs):
    color_values = [16, 17, 18, 19, 20, 21, 26, 27,
                    32, 33, 38, 39, 44, 45, 50, 51, 123, 231]

    current_guess = {}
    try:
        for current in starting_possibilities:
            guess = round(new_get_prob_by_obs(current, inputs), 5)
            if guess < 1e-5:
                guess = 0.0
            current_guess[current] = guess

        return display_data(current_guess, color_values)
    except:
        pass


def main():
    os.system("clear")
    observations = []

    mode = "test" if len(sys.argv) > 1 and sys.argv[1] == "test" else "user"

    h = random.choice(starting_possibilities) if mode == "test" else -1

    while True:
        if mode == "test":
            time.sleep(.5)
            # input()
            os.system("clear")

            d1, d2 = random.choice(dice), random.choice(dice)
            guess = h*d2 % 10 if d1 < 5 else d2

            print(colors.yellow(
                f"Rolling dice... d1:{d1}, d2:{d2}, h:{h} guess:{guess}"))

            observations.append(guess)

        elif mode == "user":
            guess = input(colors.green("Enter your guess:\n>>> "))
            if guess == '' or int(guess) not in possible_observations:
                continue
            os.system("clear")

        observations.append(int(guess))
        biggest = run_game(observations)
        is_correct_string = "--gay" if biggest == h else ''
        os.system(f"toilet -f mono12 {is_correct_string} {biggest}")

        print("\n\n\n\n\n")


if __name__ == "__main__":
    from tester import tester

    main()
