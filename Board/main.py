#!/usr/bin/python3

from random import randint
from fire import Fire

UP, DOWN, LEFT, RIGHT = "↑", "↓", "←", "→"


class Tile:
	def __init__(self, value, x, y):
		self.value = value
		self.max_value = value
		self.x, self.y = x, y

		self.path = list()

		self.neighbor_information = set()  # Tuple(Tile, Direction)

	def __str__(self):
		global current_turn

		value_color, max_color, path_color, reset = "\033[92m", "\033[94m", "\033[1;91m", "\033[0m"

		path = ' '.join(self.path)

		value = f"{value_color}{self.value}{reset}".rjust(11)
		max_value = f"{max_color}{self.max_value}{reset}".rjust(11)
		path = f"{path_color}{path}{reset}"

		return f"{value}, {max_value}, {path}".ljust(47)

	def find_neighbors_max(self):
		max_value, max_value_path = 0, list()

		for neighbor, direction in self.neighbor_information:
			neighbor_value = neighbor.max_value

			if neighbor_value > max_value:
				max_value = neighbor_value
				max_value_tile = neighbor


		# self.max_value = self.value + max_value
		# self.path = max_value_path
		
		"""
		x, y = self.x, self.y
		coords = [(x - 1, y, RIGHT), (x, y - 1, DOWN),
				  (x + 1, y, LEFT), (x, y + 1, UP)]

		max_value, max_path = 0, []

		for x, y, direction in coords:
			if board.is_tile_in_range(x, y):
				current_tile = board.get(x, y)

				if current_tile.has_been_activated and current_tile.max_value > max_value:
					max_value = current_tile.max_value
					max_path = current_tile.path + list(direction)

		self.max_value = max_value + self.value
		self.path = max_path
		"""


class Board:
	def __init__(self, length, max_value=20):
		self.length = length
		self.max_value = max_value

		self.board = list()
		self.next_tiles = set()
		self.root = None

	def __str__(self):
		string = ""

		for x in range(self.length):
			string += ''.join(
				str(self.board[x][y]) for y in range(self.length))
			string += '\n'

		return string

	def init_board(self):
		# Create the board's tiles
		for x in range(self.length):
			current_row = []

			for y in range(self.length):
				tile_value = randint(1, self.max_value)
				new_tile = Tile(tile_value, y, x)
				
				current_row.append(new_tile)
			
			self.board.append(current_row)

		# self.board = [[Tile(, y, x) for y in range(self.length)] for x in range(self.length)]

		# Update each tile to contain it's neighbors
		for x in range(self.length):
			for y in range(self.length):
				current = self.get(x, y)

				if x > 0:
					neighbor_tile = self.get(x - 1, y)
					neighbor_information = neighbor_tile, LEFT
					current.neighbor_information.add(neighbor_information)
				if y > 0:
					neighbor_tile = self.get(x, y - 1)
					neighbor_information = neighbor_tile, UP
					current.neighbor_information.add(neighbor_information)
				if x < self.length - 1:
					neighbor_tile = self.get(x + 1, y)
					neighbor_information = neighbor_tile, RIGHT
					current.neighbor_information.add(neighbor_information)
				if y < self.length - 1:
					neighbor_tile = self.get(x, y + 1)
					neighbor_information = neighbor_tile, DOWN
					current.neighbor_information.add(neighbor_information)

	def get(self, x, y=None):
		# Oops
		if y is None:
			y = x
		
		return self.board[y][x]


def step(board):
	current_tiles = board.next_tiles.copy()
	board.next_tiles = set()

	for tile in current_tiles:
		tile.find_neighbors_max()

		neighbor_tiles = [information[0] for information in tile.neighbor_information]
		board.next_tiles.update(neighbor_tiles)


def main(length=5, turns=1, board_path=None):
	global current_turn

	if board_path is not None:
		with open(board_path) as board_file:
			board_file
			pass  #board = Board()
	else:
		board = Board(length)

	board.init_board()

	# Get the middle tile (the tile we start from)
	middle = board.length // 2
	middle_tile = board.get(middle)
	board.next_tiles.add(middle_tile)

	print(board)

	for current_turn in range(1, turns + 1):
		step(board)

	max_tile = max([tile for row in board.board for tile in row],
				   key=lambda tile: tile.max_value)

	max_tile_coords = f"\033[93m({max_tile.x}|{max_tile.y})\033[0m"
	print(f"Max tile: {max_tile_coords}, {max_tile}", end="\n\n")

	print(board)


if __name__ == "__main__":
	Fire(main)
